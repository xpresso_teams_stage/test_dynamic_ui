from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, \
    IntegerField, FloatField, TextAreaField

__all__ = ["DynamicUIForm", "GenerateDynamicUIForm"]
__author = ["Mrunalini Dhapodkar"]


class DynamicUIForm(FlaskForm):
    """Dynamic UI form class."""
    predicted_result = TextAreaField('predicted_result')


class GenerateDynamicUIForm:
    """
    Class that sets attribute for UI form
    """
    def __init__(self, ui_configuration):
        self.ui_configuration = ui_configuration

    def generate_ui(self):
        """
        Sets attribute for UI form
        Returns:
            field list for jinja to render
            Dynamic UI class for predicting result

        """
        field_list = []
        for field in self.ui_configuration['fields']:
            label_name = field.get('label') if 'label' in field.keys() else field.get('name')
            field_ui_type = field.get('ui_type')
            field_data_type = field.get('data_type')
            field_list.append(label_name)
            if field_ui_type == "drop-down":
                setattr(DynamicUIForm, label_name, SelectField(label_name, choices=field['options'],
                                                               default=field.get('default')))
            elif field_ui_type == "textbox":
                if field_data_type == "integer":
                    setattr(DynamicUIForm, label_name, IntegerField(label_name, default=field.get('default')))
                elif field_data_type == "float":
                    setattr(DynamicUIForm, label_name, FloatField(label_name, default=field.get('default')))
                elif field_data_type == "string":
                    setattr(DynamicUIForm, label_name, StringField(label_name, default=field.get('default')))
        return field_list, DynamicUIForm()

